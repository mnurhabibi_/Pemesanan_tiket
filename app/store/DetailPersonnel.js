Ext.define('Pemesanan_tiket.store.DetailPersonnel', {
    extend: 'Ext.data.Store',

    alias: 'store.detailpersonnel',
    storeId: 'detailpersonnel',
    //autoLoad: true,

    fields: [
        'id_pengguna','name', 'email', 'phone', 'kota_keberangkatan', 'kota_tujuan', 'waktu', 'tanggal'
    ],

    proxy: {
        type: 'jsonp',
        api: {
            read: "http://localhost/MyApp_php/pemesanan_Tiket.php"
        },
        reader: {
            type: 'json',
            rootProperty: 'items',
            messageProperty: 'error'
        }
    }
});
