Ext.define('Pemesanan_tiket.store.Personnel', {
    extend: 'Ext.data.Store',

    alias: 'store.personnel',
    //storeId: 'personnel',
    autoLoad: true,
    //autoSync: true,

    fields: [
        'id_pengguna','name', 'email', 'phone', 'kota_keberangkatan', 'kota_tujuan', 'waktu', 'tanggal'
    ],

    proxy: {
        type: 'jsonp',
        api: {
            read: "http://localhost/MyApp_php/pemesanan_Tiket.php"
        },
        reader: {
            type: 'json',
            rootProperty: 'items',
            messageProperty: 'error'
        }
    }
});
