Ext.define('Pemesanan_tiket.view.form.Login', {
    extend: 'Ext.form.Panel',
    xtype: 'login',
    controller: 'login',
    title: 'Selamat Datang',

    requires: [
        'Pemesanan_tiket.view.form.LoginController',
        'Ext.form.Panel'
        
    ],
    shadow: true,
    cls: 'demo-solid-background',
    id: 'formlogin',
    items: [
        {
            xtype: 'fieldset',
            id: 'fieldsetlogin',
            instructions: 'Please enter the information above.',
            defaults: {
                labelWidth: '35%'
            },
            items: [
                {
                    xtype: 'textfield',
                    name: 'email',
                    label: 'Email',
                    placeHolder: 'Email',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },
                {
                    xtype: 'passwordfield',
                    revealable: true,
                    name : 'password',
                    placeHolder: 'Password',
                    label: 'Password',
                    required: true,
                    clearIcon: true
                }
            ]
        },
        {
            xtype: 'container',
            defaults: {
                xtype: 'button',
                style: 'margin: 1em',
                flex: 1
            },
            layout: {
                type: 'hbox'
            },
            items: [
                {
                    xtype: 'button',
                    text: 'Login',
                    ui: 'action',
                    handler: 'onLogin'
                    
                },
                {
                    text: 'Reset',
                    ui: 'action',
                    style: 'background-color:#E61607;margin:14px;color: white',
                    handler: function(){
                        Ext.getCmp('formlogin').reset();
                    }
                },
                {
                    text: 'Register',
                    ui: 'action',
                    style: 'background-color:#2ECC71;margin:14px;color: white',
                    handler: function (){
                        var loggedIn;
                        loggedIn = localStorage.getItem("loggedIn"); 
        
                            if (!loggedIn) {
                                    this.overlay = Ext.Viewport.add({ 
                                        xtype: 'register',
                                        floated: true,
                                        showAnimation: {
                                            type: 'popIn',
                                            duration: 250,
                                            easing: 'ease-out'
                                        },
                                        hideAnimation: {
                                            type: 'popOut',
                                            duration: 250,
                                            easing: 'ease-out'
                                        },
                                        width: "100%",
                                        height: "100%",
                                        scrollable: true
                                    });
                                    this.overlay.show();
                                }
                    }
                    
                }
            ]
        }
    ]
});