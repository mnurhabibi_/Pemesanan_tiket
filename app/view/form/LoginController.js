Ext.define('Pemesanan_tiket.view.form.LoginController', {
	extend: 'Ext.app.ViewController',
	alias: 'controller.login',

	onLogin: function() {
		var form = this.getView();
		var me = this;

		var email = form.getFields('email').getValue();
		var password = form.getFields('password').getValue();
		if(email == 'admin@gmail.com' && password == 'admin') {
			Ext.Msg.alert('Login Success', 'You have been logged in');
			form.hide();
		}
		else{
			Ext.Msg.alert('Konfirmasi', 'Username/Password Anda Salah', this).setValue();	
		}

	},
	onRegister: function (){
	    localStorage.removeItem('loggedIn');
           	this.getView().destroy();
            Ext.getCmp('formlogin').destroy();
	        this.overlay = Ext.Viewport.add({ 
	            xtype: 'login',
	            floated: true,
	            showAnimation: {
	            	type: 'popIn',
	            	duration: 250,
	            	easing: 'ease-out'
	            },
	            hideAnimation: {
	            	type: 'popOut',
	            	duration: 250,
	            	easing: 'ease-out'
	            },
	            width: "100%",
	            height: "100%",
	            scrollable: true
	        });
	        this.overlay.show();
	        Ext.Msg.alert('Register', 'Register Berhasil, Silahkan Login');
	    }
	                
});