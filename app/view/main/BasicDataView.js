Ext.define('Pemesanan_tiket.view.main.BasicDataView', {
    extend: 'Ext.Container',
    xtype: 'basicdataview',
    requires: [
        'Ext.dataview.plugin.ItemTip',
        'Ext.plugin.Responsive',
        'Pemesanan_tiket.store.Personnel',
        'Ext.field.Search'
    ],

    viewModel: {
        stores: {
            detailpersonnel: {
                type: 'detailpersonnel'
            }
        }
    },
    
    layout: 'fit',
    cls: 'ks-basic demo-solid-background',
    shadow: true,
    items: [{
            docked: 'top',
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'searchfield',
                    placeHolder: 'Search ID',
                    name: 'searchfield',
                    listeners:{
                        change: function ( me, newValue, oldValue, eOpts ) {
                            personnelStore = Ext.getStore('personnel');
                            personnelStore.filter('id_pengguna', newValue);
                        }
                    }
                }
            ]
        },{
        xtype: 'dataview',
        scrollable: 'y',
        cls: 'dataview-basic',
        /*<img src=/resources/Profil/{gambar} width=150 height=150  class="img gambar"/>*/
        itemTpl: 'Id: {id_pengguna}<br>Name: <font color="green">{name}</font><br>Email: <b>{email}</b><br>Phone: <i>{phone}</i><br>Kota Keberangkatan: {kota_keberangkatan}<br>Tujuan: {kota_tujuan}<br>Waktu : {waktu}<br>Tanggal: {tanggal}<hr>',
        bind:{
            store: '{detailpersonnel}',
        },
        plugins: {
            type: 'dataviewtip',
            align: 'l-r?',
            plugins: 'responsive',
            
            // On small form factor, display below.
            responsiveConfig: {
                "width < 600": {
                    align: 'tl-bl?'
                }
            },
            width: 600,
            minWidth: 300,
            //delegate: '.img',
            allowOver: true,
            anchor: true,
            bind: '{record}',
            tpl: '<table style="border-spacing:3px;border-collapse:separate">' + 
                    '<tr><td>ID Pengguna: </td><td>{id_pengguna}</td></tr>' +
                    '<tr><td>Name:</td><td>{name}</td></tr>' +
                    '<tr><td>Email:</td><td>{email}</td></tr>' +
                    '<tr><td>Phone:</td><td>{phone}</td></tr>' +
                    '<tr><td>Kota Keberangkatan:</td><td>{kota_keberangkatan}</td></tr>' +
                    '<tr><td>Kota Tujuan:</td><td>{kota_tujuan}</td></tr>' +
                    '<tr><td>Waktu:</td><td>{waktu}</td></tr>' +
                    '<tr><td>Tanggal:</td><td>{tanggal}</td></tr>' 
                    
        }
    }]
});