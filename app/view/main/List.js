/**
 * This view is an example list of people.
 */
Ext.define('Pemesanan_tiket.view.main.List', {
    extend: 'Ext.grid.Grid',
    xtype: 'mainlist',

    requires: [
        'Pemesanan_tiket.store.Personnel'
    ],

    title: 'Riwayat Pesanan',

    /*store: {
        type: 'personnel'
    },*/

    bind: '{personnel}',
    
    viewModel: {
        stores: {
            personnel: {
                type: 'personnel'
            }
        }
    },

    columns: [
        { text: 'ID Pengguna',  dataIndex: 'id_pengguna', width: 120 },
        { text: 'Name',  dataIndex: 'name', width: 190 },
        { text: 'Email', dataIndex: 'email', width: 250 },
        { text: 'Phone', dataIndex: 'phone', width: 150 },
        { text: 'Kota Keberangkatan', dataIndex: 'kota_keberangkatan', width: 180 },
        { text: 'Kota Tujuan', dataIndex: 'kota_tujuan', width: 180 },
        { text: 'Waktu', dataIndex: 'waktu', width: 110 },
        { text: 'Tanggal', dataIndex: 'tanggal', width: 110 }
    ],

    listeners: {
        select: 'onItemSelected'
    }
});
