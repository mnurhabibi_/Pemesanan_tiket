/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting causes an instance of this class to be created and
 * added to the Viewport container.
 *
 * TODO - Replace the content of this view to suit the needs of your application.
 */
Ext.define('Pemesanan_tiket.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',
    id: 'app-main',

    requires: [
        'Ext.MessageBox',

        'Pemesanan_tiket.view.main.MainController',
        'Pemesanan_tiket.view.main.MainModel',
        'Pemesanan_tiket.view.main.List',
        'Pemesanan_tiket.view.main.Home',
        'Pemesanan_tiket.view.main.BasicDataView',
        'Pemesanan_tiket.view.main.FormTiket'
    ],

    controller: 'main',
    viewModel: 'main',

    defaults: {
        tab: {
            iconAlign: 'top'
        },
        styleHtmlContent: true
    },

    tabBarPosition: 'bottom',

    items: [
        {
            docked: 'top',
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Read',
                    ui: 'action',
                    scope: this,
                    listeners: {
                        tap: 'onReadClicked'
                    }
                }
            ]
        },
        {
            title: 'Home',
            iconCls: 'x-fa fa-home',
            layout: 'fit',
            // The following grid shares a store with the classic version's grid as well!
            items: [{
                xtype: 'home'
            }]
        },{
            title: 'Riwayat',
            iconCls: 'x-fa fa-bookmark',
            layout: 'fit',
            // The following grid shares a store with the classic version's grid as well!
            items: [{
                xtype: 'panel',
                layout: 'vbox',
                items: [{
                    xtype: 'mainlist',
                    flex: 1
                },{
                    xtype: 'basicdataview',
                    flex: 1
                }]
                
            }]
        },{
            title: 'Pesan Ticket',
            iconCls: 'x-fa fa-search',
            layout: 'fit',
            // The following grid shares a store with the classic version's grid as well!
            items: [{
                xtype: 'myform'
            }]
        },{
            title: 'Info',
            iconCls: 'x-fa fa-info-circle',
            bind: {
                html: '<font size=5 color="green">Aplikasi ini adalah aplikasi pemesanan tiket travel secara online.</fort>'
            }
        }
    ]
});
