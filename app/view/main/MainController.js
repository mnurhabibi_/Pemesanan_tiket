/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('Pemesanan_tiket.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

    onItemSelected: function (sender, record) {
        Ext.getStore('detailpersonnel').getProxy().setExtraParams({
            name : record.data.name
        });
        Ext.getStore('detailpersonnel').filter('name', record.data.name);
        Ext.getStore('detailpersonnel').load();
    },

    onConfirm: function (choice) {
        if (choice === 'yes') {
            localStorage.removeItem('loggedIn');
            this.getView().destroy();
            Ext.getCmp('formlogin').destroy();
            this.overlay = Ext.Viewport.add({ //menambahan viewport
                xtype: 'login',
                floated: true,
                showAnimation: {
                    type: 'popIn',
                    duration: 250,
                    easing: 'ease-out'
                },
                hideAnimation: {
                    type: 'popOut',
                    duration: 250,
                    easing: 'ease-out'
                },
                width: "100%",
                height: "100%",
                scrollable: true
            });
            this.overlay.show();
        }
    },

    onConfirm: function (choice) {
        if (choice === 'yes') {
            //
        }
    },
    /*onReadClicked: function(){
        Ext.getStore('personnel').load();
    }*/
});
